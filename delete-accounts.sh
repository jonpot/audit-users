#!/bin/bash

# Check for input file.
if [[ $# -ne 2 ]]; then
    echo "Usage: ./delete-accounts.sh <FILE_ACCT_TO_DELETE> <FILE_ANSIBLE_USERS>"

else
    while IFS=, read -r f1 f2
    do
        sed -i "/$f1:/d" $2
    done < "$1"

fi
