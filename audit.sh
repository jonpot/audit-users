#!/bin/bash

MIN_REGULAR_USER_ID=1000
PASSWD_FILE="/etc/passwd"
#PASSWD_FILE="./test/passwd.test"
OU_PEOPLE_DN_SUFFIX="ou=People,dc=umich,dc=edu"

while read ROW; do

    USER_ID=$(echo ${ROW} | cut -d: -f 3)
    USER=$(echo ${ROW} | cut -d: -f 1)

    if [[ "${USER_ID}" -gt "${MIN_REGULAR_USER_ID}" ]]; then            # Skip system accounts.

        sleep 0.1
        LDAP_ENTRY=$(ldapsearch -x -LLL -h ldap.umich.edu \
            -b "${OU_PEOPLE_DN_SUFFIX}" "(uid=${USER})")                # Get user ldap entry.

        if [[ "${LDAP_ENTRY,,}" == *"dn: uid=${USER,,}"* ]]; then       # User is in ldap.

            OU_COUNT=$(echo ${LDAP_ENTRY} \
                | tr " " "\n" | grep -c "ou:")
            if [[ "${OU_COUNT}" -eq "0" ]]; then                        # User not in any OU.
                echo "${USER},1,Member of no OU."
            elif [[ "${OU_COUNT}" -eq "1" && \
                    "${LDAP_ENTRY,,}" == *"ou: alumni"* ]]; then        # User in alumni OU only.
                echo "${USER},2,Member of Alumni OU only."
            fi

        else
            echo "${USER},3,Not in LDAP."                               # User is not in ldap.
        fi

    fi

done < ${PASSWD_FILE}
