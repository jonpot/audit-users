# Audit Cavium ThunderX Hadoop User Accounts

### Removing Inactive Accounts from ThunderX

Over time, as users of ThunderX terminate their active relationship with the university, we need their user accounts removed from ThunderX. This script solves the problem of identifying and removing these user accounts in cases where the support team has not been notified of the termination.

The scripts work by comparing the ThunderX user accounts in `/etc/passwd` to the current status of the accounts from the U-M LDAP service. User accounts that are either not present in LDAP, or are members of the Alumni OU only, or are members of no OU, are written to stdout indicating they should probably be removed. The output of the script should be redirected to a file, and reviewed carefully.

```bash
# Run the script from a cavium-rm01.
ssh cavium-rm01
git clone git@bitbucket.org:jonpot/audit-users.git

cd audit-users
TO_BE_DELETED_FILE=./output-$(date "+%Y%m%d").csv
./audit.sh > ${TO_BE_DELETED_FILE}
```

Additionally, we usually do not remove any user accounts of students who are in the umsi699 groups because the professor tells the students they will continue to have access to complete their assignments. You can create a list of these users with `grep si699 /etc/group | cut -d: -f 4 | tr ',' '\n' | sort | uniq > exclude.txt`.

And, you can double-check the log files of each login server to see if any of the users have logged in recently (the log files are only kept for about 4 weeks). The commands below also check if any of the users own any running processes. These commands must be run as root on each login server in order to access the logs. After running the commands, you can manually add any users identified to the `exclude.txt` file.

```bash
sudo -s
ssh cavium-thunderx-login01

su - jonpot
TO_BE_DELETED_FILE=./output-$(date "+%Y%m%d").csv
cp workspace/audit-users/${TO_BE_DELETED_FILE} /tmp/
exit

TO_BE_DELETED_FILE=/tmp/output-$(date "+%Y%m%d").csv
for USER in $(cat ${TO_BE_DELETED_FILE} | cut -d, -f 1); do
    if grep -q ${USER} /var/log/secure*; then
        echo "WARNING: ${USER} found in recent /var/log/secure logs."
    fi
    if ps -u ${USER} > /dev/null 2>&1; then
        echo "WARNING: ${USER} found with running processes."
    fi
done

# Repeat for login02
exit
ssh cavium-thunderx-login02
...

# Exit back to cavium-rm01 and regular user
exit
exit
```

Then remove the users that are in the `exclude.txt` file from the list of to-be-deleted users file.

```bash
EXCLUDE_FILE=./exclude.txt
for USER in $(cat ${EXCLUDE_FILE}); do
    if grep -q ${USER} ${TO_BE_DELETED_FILE}; then
        echo "Excluding ${USER} from list."
        sed -i "/${USER}/d" ${TO_BE_DELETED_FILE}
    fi
done
```

The to-be-deleted users should be emailed with a notice that their accounts will be removed. Print the list of user email addresses with `cat ${TO_BE_DELETED_FILE} | cut -d, -f 1 | sed -e 's/$/@umich.edu/'`.

There is a second script that will parse the to-be-deleted file as input and remove these accounts from the Ansible user accounts file. Then the ansible file can be applied to the ThunderX system which will remove the accounts from `/etc/passwd` but leave the users home directories `/home/$USER` and their HDFS files `/user/$USER` unchanged.

```bash
ssh flux-admin09.arc-ts.umich.edu

# Create a new hotfix branch in ansible
cd /var/ansible/<UNIQNAME>/ansible
git checkout master
git pull
git checkout -b hotfix/removeStaleUsers

# Get audit scripts.
cd ~/workspace/
git clone git@bitbucket.org:jonpot/audit-users.git
cd audit-users/

# Retrieve TO_BE_DELETED_FILE from cavium-rm01 if not included in repo
export TO_BE_DELETED_FILE=<TO_BE_DELETED_FILE>
scp <UNIQNAME>@cavium-rm01:~/workspace/audit-users/${TO_BE_DELETED_FILE} ./

# The command below will remove the users from caviumHadoopUserAccts. Review the
# ansible user accounts file for accuracy and rerun the playbook to apply.
./delete-accounts.sh ${TO_BE_DELETED_FILE} \
  /var/ansible/<UNIQNAME>/ansible/group_vars/caviumHadoop/caviumHadoopUserAccts

cd /var/ansible/<UNIQNAME>/ansible/
git add -p .
git commit -m "Remove stale hadoop users"
git push origin hotfix/removeStaleUsers

# Create PR in web browser. Configure it to delete branch on merge.
# After the PR has been approved, merged, and pulled to flux-admin09,
# push the change with account-mgmt playbook.

sudo -s
cd /etc/ansible
git checkout master
git pull
ansible-playbook -i dsi-prod -l caviumHadoop utilityPlaybook/account-mgmt.yml
```

### Synchronizing ThunderX Accounts with MCommunity Groups

Our processes for creating a user account on ThunderX includes adding the account to the MCommunity Group `hadoop-users-flux-thunderx` at the time of account creation. However, we have found that sometimes the process is not followed resulting in some ThunderX users missing emails sent to the `hadoop-users-flux-thunderx` group. This script solves this problem by adding all ThunderX user accounts to the MCommunity group excluding service accounts. Note that the script currently does not remove any users from the group that do not have a corresponding account on ThunderX. This because we do not know if someone without a ThunderX account has purposefully joined the MCommunity Group to just get the emails.

The `update-group.sh` script generates an output file `modify.ldif` containing the users to be added to the group. After running the script, review this file carefully, then apply it with `ldapmodify`.

```bash
# Update the MCommunity Group.
# Run the script from any cavium node including login nodes.
./update-group.sh
Enter LDAP Password:
Gathered list of current members of the MCommunity group hadoop-users-flux-thunderx.
Identifying users that need to be added to the group. This can take 5 minutes.
....................................................................................................................................................................................................................
....................................................................................................................................................................................................................
....................................................................................................................................................................................................................
......................................................................................................................
Created file modify.ldif containing users to be added to MCommunity
  group. Review the file and apply it if it is correct with this command.

ldapmodify -H 'ldaps://ldap.umich.edu' -D 'uid=jonpot,ou=people,dc=umich,dc=edu' -W -f modify.ldif
```
