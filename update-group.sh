#!/bin/bash
# Updates the MCommunity Group for ThunderX users so that it matches the list 
# of current users of ThunderX system. Over time as users are added and deleted 
# from ThunderX, the MCommunity Group can get out of sync.


#GROUP_NAME="hadoop-test"
GROUP_NAME="hadoop-users-flux-thunderx"
CURRENT_GROUP_MEMBERS_LIST="group-members.txt"
LDIF_FILE="modify.ldif"
MIN_REGULAR_USER_ID=1000
ME=$(whoami)
PASSWD_FILE="/etc/passwd"
#PASSWD_FILE="passwd-modified2"

# Get all members of the group (iamGroup) MCommunity Group and save in file.
ldapsearch -LLL -H "ldaps://ldap.umich.edu" \
    -D "uid=${ME},ou=people,dc=umich,dc=edu" -W \
    -b "ou=user groups,ou=groups,dc=umich,dc=edu" "(cn=${GROUP_NAME})" member | \
    grep "member:" | awk '{print$2}' | cut -d, -f 1 | cut -d= -f 2 > ${CURRENT_GROUP_MEMBERS_LIST}
echo "Gathered list of current members of the MCommunity group ${GROUP_NAME}."

# Identify ThunderX users that are not in the group and add them to ldif file.
> ${LDIF_FILE}

echo "Identifying users that need to be added to the group. This can take 5 minutes."
while read ROW; do                                              # For each user in /etc/passwd.

    USER_ID=$(echo ${ROW} | cut -d: -f 3)                       # Get this user info from passwd file.
    USER=$(echo ${ROW} | cut -d: -f 1)

    if [[ "${USER_ID}" -gt "${MIN_REGULAR_USER_ID}" ]]; then    # Skipping system accounts with low UIDs.
        echo -n "."
        grep -iq ${USER} ${CURRENT_GROUP_MEMBERS_LIST}
        GROUP_MEMBER_TEST=$?
        if [[ "${GROUP_MEMBER_TEST}" -eq "1" ]]; then           # If not member of group, prepare to add to modify file.
            sleep 0.2                                           # But first, test if user is in LDAP.
            ldapsearch -x -LLL -h ldap.umich.edu \
                -b "ou=People,dc=umich,dc=edu" "(uid=$USER)" dn | grep -q dn
            if [[ $? -eq "0" ]]; then                           # If in LDAP, add user to modify file.
                echo "dn: cn=${GROUP_NAME},ou=User Groups,ou=Groups,dc=umich,dc=edu" >> ${LDIF_FILE}
                echo "changetype: modify" >> ${LDIF_FILE}
                echo "add: member" >> ${LDIF_FILE}
                echo -e "member: uid=${USER},ou=people,dc=umich,dc=edu\n" >> ${LDIF_FILE}
            fi
        elif [[ "${GROUP_MEMBER_TEST}" -eq "0" ]]; then         # Skip if already member of the group.
            true
        else
            echo "ERROR: grep returned an unexpected exit code."
            exit
        fi
    fi

done < ${PASSWD_FILE}
echo "."

# Manually apply the ldif file adding the ThunderX users to the group.
echo "Created file ${LDIF_FILE} containing users to be added to MCommunity"
echo -e "  group. Review the file and apply it if it is correct with this command.\n"

echo "ldapmodify -H 'ldaps://ldap.umich.edu' -D 'uid=${ME},ou=people,dc=umich,dc=edu' -W -f ${LDIF_FILE}"

# Identify accounts on ThunderX that are not in the MCommunity group.
# Add accounts to MCommunity group.

###########################################################################
# Identify accounts in the MCommunity group that are not on ThunderX.
# Not sure if these should be modified because perhaps some people want to 
# get emails regarding ThunderX, but do not have accounts.
###########################################################################
